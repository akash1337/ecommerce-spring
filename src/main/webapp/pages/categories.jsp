<%@ page import="com.example.ecommercespring.model.Category" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Categories</title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Ecommerce</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                    <a class="nav-link" href="/products">Products</a>
                    <a class="nav-link" href="/categories">Categories</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="alert alert-${alertClass}" role="alert">
        ${alertMsg}
    </div>

    <form action="/categories" method="post">
        <div class="row g-3 align-items-center">
            <input type="hidden" name="id" value="${category != null ? category.getId() : 0}">
            <div class="col-auto">
                <label for="name" class="col-form-label">Name</label>
            </div>
            <div class="col-auto">
                <input type="text" id="name" name="name" class="form-control" value="${category.getName()}">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">${category == null ? "Add" : "Update"}
                </button>
            </div>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr class="d-flex">
            <th class="col-1" scope="col">#</th>
            <th class="col-3" scope="col">Name</th>
            <th class="col-2" scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <jsp:useBean id="categories" scope="request"
                     type="org.springframework.data.domain.Page<com.example.ecommercespring.model.Category>"></jsp:useBean>
        <% for (Category category : categories) { %>
        <tr class="d-flex">
            <td class="col-1" scope="row">
                <%= category.getId() %>
            </td>
            <td class="col-3">
                <%= category.getName() %>
            </td>
            <td class="col-2">
                <div class="row">
                    <div class="col-4">
                        <a href="/categories/<%= category.getId() %>" class="btn btn-primary" tabindex="-1"
                           role="button"><i class="far fa-edit"></i></a>
                    </div>
                    <div class="col-4">
                        <form action="/categories/<%= category.getId() %>" method="post">
                            <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination float-right">
            <li class="page-item <%= categories.isFirst() ? "disabled" : "" %>">
                <a class="page-link" href="?page=<%= categories.getNumber() - 1 %>">Previous</a>
            </li>
            <%
                int totalPages = categories.getTotalPages();
                for (int i = 0; i < totalPages; i++) {
            %>
            <li class="page-item <%= categories.getNumber() == i ? "active": "" %>">
                <a class="page-link" href="?page=<%=i%>"><%=i + 1%>
                </a>
            </li>
            <%}%>
            <li class="page-item <%= categories.isLast() ? "disabled": "" %>">
                <a class="page-link" href="?page=<%=categories.getNumber() + 1 %>">Next</a>
            </li>
        </ul>
    </nav>

</div>
</body>
</html>