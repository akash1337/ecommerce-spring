<%@ page import="com.example.ecommercespring.model.Product" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="com.example.ecommercespring.model.Category" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Products</title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Ecommerce</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                    <a class="nav-link" href="/products">Products</a>
                    <a class="nav-link" href="/categories">Categories</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="alert alert-${alertClass}" role="alert">
        ${alertMsg}
    </div>
    <div class="row align-items-start">
        <div class="col-8">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Categories</th>
                    <th scope="col">Mfg Date</th>
                    <th scope="col">Exp Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <jsp:useBean id="products" scope="request"
                             type="org.springframework.data.domain.Page<com.example.ecommercespring.model.Product>"/>

                <% for (Product product : products) { %>
                <tr>
                    <td scope="row">
                        <%= product.getId() %>
                    </td>
                    <td>
                        <%= product.getName() %>
                    </td>
                    <td>
                        <%= product.getCategories().stream().map(c -> c.getName()).collect(Collectors.joining("<br/>")) %>
                    </td>
                    <td>
                        <%= product.getMfgDate().toString() %>
                    </td>
                    <td>
                        <%=product.getExpDate().toString()%>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-sm-4">
                                <a href="/products/<%= product.getId() %>" class="btn btn-primary" tabindex="-1"
                                   role="button"><i class="far fa-edit"></i></a>
                            </div>
                            <div class="col-sm-4">
                                <form action="/products/<%= product.getId() %>" method="post">
                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
            <nav aria-label="Page navigation">
                <ul class="pagination float-right">
                    <li class="page-item <%= products.isFirst() ? "disabled" : "" %>">
                        <a class="page-link" href="?page=<%= products.getNumber() - 1 %>">Previous</a>
                    </li>
                    <%
                        int totalPages = products.getTotalPages();
                        for (int i = 0; i < totalPages; i++) {
                    %>
                    <li class="page-item <%= products.getNumber() == i ? "active": "" %>">
                        <a class="page-link" href="?page=<%=i%>"><%=i + 1%>
                        </a>
                    </li>
                    <%}%>
                    <li class="page-item <%= products.isLast() ? "disabled": "" %>">
                        <a class="page-link" href="?page=<%=products.getNumber() + 1 %>">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-4">
            <form action="/products" method="post">
                <input type="hidden" name="id" value="${product != null ? product.getId() : 0}">
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" id="name" value="${product.getName()}" name="name" class="form-control"
                           required>
                </div>
                <div class="mb-3">
                    <label for="mfgDate" class="col-form-label">MFG Date</label>
                    <input type="date"
                           value="${product != null ? product.getMfgDate().toString().substring(0, 10) : ""}"
                           id="mfgDate" name="mfgDate"
                           class="form-control"
                           required>
                </div>
                <div class="mb-3">
                    <label for="expDate" class="col-form-label">Exp Date</label>
                    <input type="date"
                           value="${product != null ? product.getExpDate().toString().substring(0, 10) : ""}"
                           id="expDate" name="expDate"
                           class="form-control"
                           required>
                </div>
                <div class="mb-3">
                    <jsp:useBean id="categories" scope="request"
                                 type="java.util.List<com.example.ecommercespring.model.Category>"/>
                    <label for="categories" class="col-form-label">Categories</label>
                    <select id="categories" name="categories" size="3" class="form-select" multiple>
<%--                        <option value="0" ${ product != null && product.getCategories().size() > 0 ? "" : "selected"} >--%>
<%--                            No Category--%>
<%--                        </option>--%>
                        <% for (Category c : categories) { %>
                        <option ${ product != null && product.getCategories().contains(c) ? "selected" : ""}
                                value="<%= c.getId() %>"><%= c.getName() %>
                        </option>
                        <% } %>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">${product == null ? "Add" : "Update"}</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>