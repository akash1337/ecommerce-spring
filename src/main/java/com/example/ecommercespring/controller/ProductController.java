package com.example.ecommercespring.controller;

import com.example.ecommercespring.dao.CategoryRepo;
import com.example.ecommercespring.model.Category;
import com.example.ecommercespring.model.Product;
import com.example.ecommercespring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller(value = "/products")
public class ProductController {

    @Autowired
    ProductService service;

    @Autowired
    CategoryRepo categoryRepo;

    @GetMapping("/products")
    public ModelAndView getProducts(Integer page) {
        ModelAndView mv = new ModelAndView("products");
        if (page == null) {
            page = 0;
        }
        Page<Product> products = service.getByPage(page);
        mv.addObject("products", products);

        List<Category> categories = categoryRepo.findAll();
        mv.addObject("categories", categories);
        return mv;
    }

    @PostMapping("/products")
    public ModelAndView saveProduct(Product product) {
        service.save(product);
        ModelAndView mv = getProducts(0);
        mv.addObject("alertClass", "success");
        mv.addObject("alertMsg", "Product Saved");
        return mv;
    }

    @GetMapping("/products/{id}")
    public ModelAndView getProduct(@PathVariable int id) {
        ModelAndView mv = getProducts(0);

        Product product = service.getById(id);
        if (product != null) {
            mv.addObject("product", product);
        } else {
            mv.addObject("alertClass", "danger");
            mv.addObject("alertMsg", "Product Not Found");
        }

        return mv;
    }

    @PostMapping("/products/{id}")
    public ModelAndView deleteProduct(@PathVariable int id) {
        service.deleteById(id);
        ModelAndView mv = getProducts(0);
        mv.addObject("alertClass", "success");
        mv.addObject("alertMsg", "Product Deleted");
        return mv;
    }
}
