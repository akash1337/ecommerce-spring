package com.example.ecommercespring.controller;

import com.example.ecommercespring.model.Category;
import com.example.ecommercespring.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CategoryController {

    @Autowired
    CategoryService service;

    @GetMapping("/categories")
    public ModelAndView getCategories(Integer page) {
        ModelAndView mv = new ModelAndView("categories");
        if (page == null) {
            page = 0;
        }
        Page<Category> categories = service.getByPage(page);
        mv.addObject("categories", categories);
        return mv;
    }

    @PostMapping("/categories")
    public ModelAndView saveCategory(Category category) {
        service.save(category);
        ModelAndView mv = getCategories(0);
        mv.addObject("alertClass", "success");
        mv.addObject("alertMsg", "Category Saved");
        return mv;
    }

    @GetMapping("/categories/{id}")
    public ModelAndView getCategory(@PathVariable int id) {
        ModelAndView mv = getCategories(0);

        Category category = service.getById(id);
        if (category != null) {
            mv.addObject("category", category);
        } else {
            mv.addObject("alertClass", "danger");
            mv.addObject("alertMsg", "Category Not Found");
        }

        return mv;
    }

    @PostMapping("/categories/{id}")
    public ModelAndView deleteCategory(@PathVariable int id) {
        service.deleteById(id);
        ModelAndView mv = getCategories(0);
        mv.addObject("alertClass", "success");
        mv.addObject("alertMsg", "Category Deleted");
        return mv;
    }
}
