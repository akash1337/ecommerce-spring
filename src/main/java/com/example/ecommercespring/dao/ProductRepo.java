package com.example.ecommercespring.dao;

import com.example.ecommercespring.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepo extends JpaRepository<Product, Integer> {
}
