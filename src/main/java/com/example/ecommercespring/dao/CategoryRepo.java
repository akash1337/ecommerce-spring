package com.example.ecommercespring.dao;

import com.example.ecommercespring.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepo extends JpaRepository<Category, Integer> {
}
