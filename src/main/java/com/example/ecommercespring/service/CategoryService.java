package com.example.ecommercespring.service;

import com.example.ecommercespring.dao.CategoryRepo;
import com.example.ecommercespring.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    @Autowired
    CategoryRepo repo;

    private int pageLimit = 2;

    public Category getById(int id) {
        return repo.findById(id).orElse(null);
    }

    public Page<Category> getByPage(int page) {
        return repo.findAll(PageRequest.of(page, pageLimit));
    }

    public void save(Category category) {
        repo.save(category);
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public void setPageLimit(int limit) {
        pageLimit = limit;
    }
}
