package com.example.ecommercespring.service;

import com.example.ecommercespring.dao.ProductRepo;
import com.example.ecommercespring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    ProductRepo repo;

    private int pageLimit = 2;

    public Product getById(int id) {
        return repo.findById(id).orElse(null);
    }

    public Page<Product> getByPage(int page) {
        return repo.findAll(PageRequest.of(page, pageLimit));
    }

    public void save(Product product) {
        repo.save(product);
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public void setPageLimit(int limit) {
        pageLimit = limit;
    }
}
