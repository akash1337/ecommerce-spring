create database ecommerce;
use ecommerce;

create table product (
      id int auto_increment,
      name varchar(255),
      mfg_date date,
      exp_date date,
      primary key (id)
);

create table category (
    id int auto_increment,
    name varchar(255),
    primary key (id)
);

create table product_category (
      id int primary key auto_increment,
      product_id int,
      category_id int,
      foreign key (product_id) references product(id),
      foreign key (category_id) references category(id) on update cascade on delete cascade
);